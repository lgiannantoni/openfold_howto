The `of_modules` file is a collection of `lmod` modules.

- Copy it under your `~/.config/lmod/` path
- Load the modules: `module restore of_modules`
- Proceed to [install OpenFold Doctor](https://github.com/KosinskiLab/openfold-doctor?tab=readme-ov-file#-installation)

Caution: always restore the modules first, then activate the conda env. Otherwise, you could end up using the wrong python.